class Span:
    def __init__(self, _from, _to):
        self.from_inclusive = _from
        self.to_exclusive = _to

    def center(self):
        return (self.from_inclusive + self.to_exclusive) / 2

    def width(self):
        return self.to_exclusive - self.from_inclusive

    def get_from_to(self):
        return self.from_inclusive, self.to_exclusive

    def get_from_width(self):
        return self.from_inclusive, self.to_exclusive - self.from_inclusive

    @classmethod
    def union(cls, spans):
        min_from, max_to = None, None
        for s in spans:
            if min_from is None:
                min_from, max_to = s.from_inclusive, s.to_exclusive
            else:
                min_from = min(min_from, s.from_inclusive)
                max_to = max(max_to, s.to_exclusive)
        return Span(min_from, max_to)

    @classmethod
    def intersect(cls, spans):
        max_from, min_to = None, None
        for s in spans:
            if max_from is None:
                max_from, min_to = s.from_inclusive, s.to_exclusive
            else:
                max_from = max(max_from, s.from_inclusive)
                min_to = min(min_to, s.to_exclusive)
        if max_from < min_to:
            return Span(max_from, min_to)
        return None

    @classmethod
    def by_from_to(cls, _from, _to):
        return Span(_from, _to)

    @classmethod
    def by_from_and_width(cls, _from, _width):
        return Span(_from, _from + _width)

    def cast_int(self):
        return Span(int(round(self.from_inclusive)), int(round(self.to_exclusive)))

    def calc_expanded(self, base_expand, from_scale = 1.0, to_scale=1.0):
        new_from = self.from_inclusive - base_expand * from_scale
        new_to = self.to_exclusive + base_expand * to_scale
        return Span.by_from_to(new_from, new_to)

    def __str__(self):
        return '[{0},{1})'.format(self.from_inclusive, self.to_exclusive)