from typing import Dict, Tuple, Any

class Choices:
    def __init__(self, options_weighted):
        self.__build(options_weighted)

    def __build(self, options_weighted):
        if options_weighted is None:
            return
        choices = {}
        normal_weights = None
        if isinstance(options_weighted, (list, tuple, set)):
            default_weight = 1
            standard_options = []
            for item in options_weighted:
                if isinstance(item, (list, tuple)):
                    standard_item = (item[0], item[1])
                elif 'weight' in dir(item):
                    weight = item.weight
                    standard_item = (item, weight)
                else:
                    standard_item = (item, default_weight)
                standard_options.append(standard_item)
            options, weights = zip(*standard_options)
            total_weight = sum(weights)
            normal_weights = [w / total_weight for w in weights]
            accum_weight = 0
            for op_weighted in standard_options:
                _op, _w = op_weighted
                accum_weight += _w
                choices[accum_weight / total_weight] = _op
        elif isinstance(options_weighted, dict):
            total_weight = sum(options_weighted.values())
            normal_weights = [w / total_weight for w in options_weighted.values()]
            options = tuple(options_weighted.keys())
            accum_weight = 0
            for _op in options_weighted:
                accum_weight += options_weighted[_op]
                choices[accum_weight / total_weight] = _op
            pass
        else:
            raise AssertionError()

        self.__choices = choices
        self.__choice_keys = sorted(choices.keys())
        self.__choice_key_len = len(self.__choice_keys)
        self.__options = options
        self.__option_weights = normal_weights

    def options(self, with_weight=False):
        return tuple(zip(self.__options, self.__option_weights)) if with_weight else self.__options

    def option_count(self) -> int:
        return len(self.__options)

    def option_index(self, option):
        return self.__options.index(option)

    def option(self, index, with_weight=False):
        return (self.__options[index], self.__option_weights[index]) if with_weight else self.__options[index]

    def random_choice(self, with_index=False):
        import random
        option_index, option = self.__get_choice__(random.random())
        return (option, option_index) if with_index else option

    def __get_choice__(self, normal_key) -> Tuple[int, Any]:
        import bisect
        choice_key_index = bisect.bisect_left(self.__choice_keys, normal_key)
        if choice_key_index == self.__choice_key_len:
            return None
        choice_key = self.__choice_keys[choice_key_index]
        choice = self.__choices[choice_key]
        return choice_key_index, choice

    def __getitem__(self, key):
        _, op = self.__get_choice__(key)
        return op

    def __str__(self):
        processed = 0.0
        percents = []
        for r in self.__choice_keys:
            percents.append(r - processed)
            processed = r
        lines =[]
        for i in range(self.__choice_key_len):
            key = self.__choice_keys[i]
            percent = percents[i]
            data = self.__choices.get(key)
            lines.append('[{0:.1%} {1}]'.format(percent, str(data)))
        return ' '.join(lines)
