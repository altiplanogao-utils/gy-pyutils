class Rect:
    def __init__(self, left, top, width, height):
        self._left = left
        self._top = top
        self._width = width
        self._height = height

    def left(self):
        return self._left
    def top(self):
        return self._top
    def width(self):
        return self._width
    def height(self):
        return self._height
    def right(self):
        return self._left + self._width
    def bottom(self):
        return self._top + self._height

    def x_center(self):
        return self._left + self._width / 2
    def y_center(self):
        return self._top + self._height / 2

    def to_array(self):
        return [self._left, self._top, self._width, self._height]
    def get_center(self):
        return (self.x_center(), self.y_center())
    def get_lt(self):
        return (self._left, self._top)
    def get_lb(self):
        return (self._left, self.bottom())
    def get_rt(self):
        return (self.right(), self._top)
    def get_rb(self):
        return (self.right(), self.bottom())
    def get_ltrb(self):
        return (self._left, self._top, self.right(), self.bottom())

    def _xywh(self):
        return self._left, self._top, self._width, self._height

    @classmethod
    def union(cls, rects):
        min_x, max_x, min_y, max_y = None, None, None, None
        for fill in rects:
            x, y, w, h = fill._xywh()
            if min_x is None:
                min_x, min_y, max_x, max_y = x, y, x + w, y + h
            else:
                min_x = min(x, min_x)
                min_y = min(y, min_y)
                max_x = max(x + w, max_x)
                max_y = max(y + h, max_y)
        return Rect.by_xxyy(min_x, max_x, min_y, max_y)

    @classmethod
    def intersect(cls, rects):
        min_x, max_x, min_y, max_y = None, None, None, None
        for fill in rects:
            x, y, w, h = fill._xywh()
            if min_x is None:
                min_x, min_y, max_x, max_y = x, y, x + w, y + h
            else:
                min_x = max(x, min_x)
                min_y = max(y, min_y)
                max_x = min(x + w, max_x)
                max_y = min(y + h, max_y)
        return Rect.by_xxyy(min_x, max_x, min_y, max_y)

    @classmethod
    def by_xywh(cls, x, y, w, h):
        return Rect(x, y, w, h)

    @classmethod
    def by_xy_wh(cls, xy, wh):
        x, y = xy
        w, h = wh
        return Rect(x, y, w, h)

    @classmethod
    def by_yxhw(cls, y, x, h, w):
        return Rect(x, y, w, h)

    @classmethod
    def by_yx_hw(cls, yx, hw):
        y, x = yx
        h, w= hw
        return Rect(x, y, w, h)

    @classmethod
    def by_xxyy(cls, x_start, x_end, y_start, y_end):
        return Rect(x_start, y_start, x_end - x_start, y_end - y_start)

    @classmethod
    def by_array(cls, array):
        return Rect(array[0], array[1], array[2], array[3])

    def calc_expaned(self, base_expand,
                     left_scale = 1.0, right_scale = 1.0,
                     top_scale = 1.0, bottom_scale = 1.0):
        left = self.left() - base_expand * left_scale
        right = self.right() + base_expand * right_scale
        top = self.top() - base_expand * top_scale
        bottom = self.bottom() + base_expand * bottom_scale
        return Rect.by_xxyy(left,right,top,bottom)

    def cast_int(self):
        return Rect.by_xywh(int(round(self._left)), int(round(self._top)), int(round(self._width)), int(round(self._height)))

    def get_xy(self):
        return self._left, self._top
    def get_yx(self):
        return self._top, self._left
    def get_hw(self):
        return self._height, self._width

    def get_wh(self):
        return self._width, self._height

    def get_yx_hw(self):
        return ((self._top, self._left), (self._height, self._width))

    def get_xy_wh(self):
        return ((self._left,self._top), (self._width, self._height))

    def get_vertexes(self, clock_wise=True, first_index_from_tl=0):
        (top, left), (h, w) = self.get_yx_hw()
        bottom, right = top + h, left + w
        vertexes = [(top, left), (top, right), (bottom, right), (bottom, left)]
        if clock_wise:
            if first_index_from_tl == 0:
                return vertexes
            indices = [(i + first_index_from_tl)%4 for i in range(4)]
        else:
            indices = [(i + 4 - first_index_from_tl)%4 for i in range(4)]
        return [vertexes[indices[i]] for i in range(4)]

    def __str__(self):
        return '{0},{1} {2}x{3}'.format(self._left, self._top, self._width, self._height)
