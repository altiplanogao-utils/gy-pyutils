from abc import ABC, abstractmethod

class Average(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def reset(self):
        pass

    @abstractmethod
    def apply(self, value, times=1):
        pass

    @abstractmethod
    def get(self):
        pass


class TotalAverageCalculator(Average):
    def __init__(self):
        self.__count = 0
        self.__average = 0

    def reset(self):
        self.__count = 0
        self.__average = 0

    def apply(self, value, times=1):
        total = value * times + self.__average * self.__count
        self.__count = times + self.__count
        if self.__count > 0:
            self.__average = total / self.__count
        else:
            self.__average = 0

    def get(self):
        return self.__average


class MovingAverage(Average):
    def __init__(self, window):
        self.__window = window
        self.__data = []
        self.__moving_sum = 0

    def reset(self):
        self.__data = []
        self.__moving_sum = 0

    def apply(self, value, times=1):
        for i in range(times):
            self.__applyOne(value)
        return self.get()

    def __applyOne(self, value):
        if len(self.__data) < self.__window:
            self.__data.append(value)
            self.__moving_sum += value
        else:
            pop = self.__data[0]
            self.__data = self.__data[1:] + [value,]
            self.__moving_sum = self.__moving_sum - pop + value

    def get(self):
        dataLen = len(self.__data)
        return self.__moving_sum / dataLen if dataLen > 0 else 0


class ExponentialMovingAverage(Average):
    def __init__(self, window=100, beta=None, first_as_initial=False):
        self.__first_as_initial = first_as_initial;
        self.__current = None if first_as_initial else 0.0
        if isinstance(beta, float):
            if 0 < beta < 1:
                self.__beta = beta
            else:
                raise Exception('arg error')
        else:
            self.__beta = 1.0 - 1 / window

    def reset(self):
        self.__current = None

    def apply(self, value, times=1):
        if self.__current is None:
            if self.__first_as_initial:
                self.__current = value
                return self.__current;
            else:
                self.__current = 0
        temp = self.__current
        for i in range(times):
            temp = self.__beta * temp + (1 - self.__beta) * value
        self.__current = temp
        return self.__current

    def get(self):
        return self.__current

