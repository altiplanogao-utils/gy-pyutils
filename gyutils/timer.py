import time

class TimeTracker:
    class _one_track:
        def __init__(self, name, main_tracker):
            self.main_tracker = main_tracker
            self.name = name
            self.start = -1

        def __enter__(self):
            self.start = time.perf_counter()

        def __exit__(self, exc_type, exc_val, exc_tb):
            cost = time.perf_counter() - self.start
            if self.start < 0:
                cost = 0
            self.main_tracker.__one_track_exit__(self.name, cost)

    class record:
        def __init__(self):
            self.hits = 0
            self.cost = 0.0
        def add_cost(self, time_cost):
            self.hits += 1
            self.cost += time_cost
            return self
        def each(self):
            if self.hits == 0:
                return 0
            else:
                return self.cost / self.hits

    def __init__(self, start=True):
        self._named_ = {}
        self._laungh = -1
        if start:
            self.start()
    def start(self):
        if self._laungh < 0:
            self._laungh = time.perf_counter()

    def track(self, name):
        self.start()
        return TimeTracker._one_track(name, self)

    def __one_track_exit__(self, one_track_name, time_cost):
        rec = self._named_.get(one_track_name, TimeTracker.record()).add_cost(time_cost)
        self._named_[one_track_name] = rec

    def costs(self):
        if self._laungh<0:
            return ''
        total_elapsed = time.perf_counter() - self._laungh
        res_named = []
        percents = 0
        for n,rec in self._named_.items():
            hits,cost, each = rec.hits, rec.cost, rec.each()
            percent = 100 * cost / total_elapsed
            percents += percent
            str = "[{0} {1:.1f}%, {2:.2f}s = {3} * {4:.2f}s]".format(n, percent, cost, hits, each)
            res_named.append(str)
        res_named.insert(0, 'Total: {0:.2f}s ({1: .1f}%)'.format(total_elapsed, percents))
        return '; '.join(res_named)

class Stopwatch(object):
    MILLISECOND = 0
    SECOND = 1
    HOUR = 2

    start_time = 0
    ids_start_time = {}

    def __init__(self, show_type=MILLISECOND):
        self.show_type = show_type
        self.start()

    def start(self, tid=None):
        if tid:
            self.ids_start_time[tid] = time.time() * 1000
        else:
            self.start_time = time.time() * 1000

    def end(self, msg, tid=None, update=False):
        end_time = time.time() * 1000

        if tid and (tid in self.ids_start_time.keys()):
            total_time = end_time - self.ids_start_time[tid]
            if update:
                self.ids_start_time[tid] = end_time
        else:
            total_time = end_time - self.start_time
            if update:
                self.start_time = end_time

        print_time = total_time
        if self.show_type == Stopwatch.MILLISECOND:
            print_time = total_time
            print_unit = 'ms'
        elif self.show_type == Stopwatch.SECOND:
            print_time = total_time / 1000
            print_unit = 's'
        elif self.show_type == Stopwatch.HOUR:
            print_time = total_time / 1000 / 60 / 60
            print_unit = 'h'

        print("%s: %.3f %s" % (msg, print_time, print_unit))
