import random

class Feeder:
    def __init__(self, candidates, repeats=1, limit=-1, repeat_each=False):
        self.candidates = candidates
        self.repeats = repeats
        self.limit = limit
        self.repeat_each = repeat_each

    @staticmethod
    def _gen(candidates, repeats, limit, repeat_each):
        generated = 0
        if repeat_each:
            for c in candidates:
                for _ in range(repeats):
                    generated += 1
                    if 0 < limit < generated:
                        return
                    yield c
        else:
            for _ in range(repeats):
                for c in candidates:
                    generated += 1
                    if 0 < limit < generated:
                        return
                    yield c

    def __iter__(self):
        return Feeder._gen(self.candidates, self.repeats, self.limit, self.repeat_each)

    def datas(self, limit=-1):
        return Feeder._gen(self.candidates, limit, False)

class RandomFeeder:
    def __init__(self, candidates):
        self.candidates = candidates

    @staticmethod
    def _gen(cands, limit):
        generated = 0
        while True:
            import copy
            shuffled = copy.copy(cands)
            random.shuffle(shuffled)
            for c in shuffled:
                if limit > 0 and generated >= limit:
                    return
                generated += 1
                yield c

    def __iter__(self):
        return RandomFeeder._gen(self.candidates, -1)

    def datas(self, limit=-1):
        return RandomFeeder._gen(self.candidates, limit)
