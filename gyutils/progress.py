import time, sys
from gyutils.average import Average, TotalAverageCalculator


class ProgressEstimator:
    def __init__(self, total, average=None):
        self.__total_step = total
        self.__done = 0
        self.__len_str_len = len(str(self.__total_step))
        self.__start_time = time.time()
        self.__last_time = self.__start_time
        self.__average = average if isinstance(average, Average) else TotalAverageCalculator()

    def kick_delta(self, delta_steps=1):
        if delta_steps > 0:
            now = time.time()
            total_cost, new_cost, self.__last_time = now - self.__start_time, now - self.__last_time, now
            self.__done += delta_steps
            self.__average.apply(value=new_cost / delta_steps, times=delta_steps)

    def kick(self, done_steps):
        delta_steps = done_steps - self.__done
        self.kick_delta(delta_steps)

    def steps_done(self):
        return self.__done

    def steps_remain(self):
        return self.__total_step - self.__done

    def steps_total(self):
        return self.__total_step

    def time_costed(self):
        return self.__last_time - self.__start_time

    def each_step_cost(self):
        return self.__average.get()

    def time_remain_estimate(self):
        return self.steps_remain() * self.__average.get()

    def time_total_estimate(self):
        return self.time_costed() * self.time_remain_estimate()

    def progress(self):
        return self.__done / self.__total_step

    def format(self, simple=True):
        def dt_str(total):
            seconds = total % 60
            minutes = int(total // 60) % 60
            hours = int(total // 3600)
            return '{0:0>2d}:{1:0>2d}:{2:04.1f}'.format(hours, minutes, seconds)

        template = '{0}/{1}({2:5.1%}) each:{3:2.2f} [{4}<{5}, Total: {6}]' if simple else '{0}/{1}({2:5.1%}) each:{3:2.2f} [Used: {4}, Total: {6}, Remaining: {5}]'

        done_str = str(self.__done).rjust(self.__len_str_len)
        return template.format(
            done_str, self.__total_step, self.progress(), self.each_step_cost(),
            dt_str(self.time_costed()),
            dt_str(self.time_remain_estimate()),
            dt_str(self.time_total_estimate())
        )

    def __str__(self):
        return self.format(simple=True)


class ProgressPrinter:
    def __init__(self, printer=sys.stdout,
                 expect_lines=-1, same_line=True,
                 extra_pre=None, extra_post=None):
        self.__printer = printer
        self.__same_line = same_line
        self.__line_done = 0
        self.__expect_lines = expect_lines
        self.__extra_pre = extra_pre
        self.__extra_post = extra_post

    def write_line(self, line):
        pre_str,post_str = None, None

        if callable(self.__extra_post):
            pre_str = self.__extra_post()
        elif isinstance(self.__extra_post, str):
            pre_str = self.__extra_post
        pre_str = '' if pre_str is None else pre_str     
        
        if callable(self.__extra_post):
            post_str = self.__extra_post()
        elif isinstance(self.__extra_post, str):
            post_str = self.__extra_post
        post_str = '' if post_str is None else post_str     
        
        line = pre_str + line + post_str

        first_line = self.__line_done == 0
        last_line = self.__line_done == (self.__expect_lines - 1) if self.__expect_lines > 0 else False
        if self.__same_line:
            if first_line:
                if last_line:
                    line = line + '\n'
                else:
                    pass
            elif last_line:
                line = '\r' + line + '\n'
            else:
                line = '\r' + line
            self.__printer.write(line)
        else:
            self.__printer.write(line + '\n')
        self.__printer.flush()
        self.__line_done = self.__line_done + 1


class IterateWithProgress:
    def __init__(self, container, average=None, printer=sys.stdout, same_line=True, element_to_str=None):
        self.__container = container
        self.__len = len(container)
        self.__estimator = ProgressEstimator(len(container), average)
        self.__printer = ProgressPrinter(sys.stdout if printer is None else printer, same_line=same_line, expect_lines = self.__len)
        self.__element_to_str = element_to_str

    def __len__(self):
        return self.__len

    def __iter__(self):
        total = self.__len
        inner_iter = iter(self.__container)

        def generator():
            done_step = 0
            while done_step < total:
                res = next(inner_iter)
                done_step = done_step + 1
                self.__estimator.kick(done_step)
                if self.__printer is not None:
                    content = str(self.__estimator)
                    if self.__element_to_str is not None:
                        content = content + ' Data:' + self.__element_to_str(res)
                    self.__printer.write_line(content)
                yield res

        return generator()
