class NDIndexWalker:
    def __init__(self, size: int, reverse: bool = True, times: int = 1):
        assert isinstance(size, (list, tuple))
        self._input_size = size
        self._reverse = reverse
        self._times = times

        self._type = type(size)
        self._size = size
        self._len = len(size)
        if reverse:
            self._size = self._type(list(reversed(list(size))))
        self._pos = [0] * (self._len + 1)

    def is_valid(self, pos):
        try:
            for i in range(self._len):
                pv = pos[i]
                pl = self._input_size[i]
                if pv < 0 or pv >= pl:
                    return False
            return True
        except Exception:
            return False

    def has_next(self):
        return self._pos[self._len] < self._times

    def progress(self):
        dcompress = 1;
        accum = 0
        for i in range(self._len):
            accum += self._pos[i] * dcompress
            dcompress *= self._size[i]
        unit_total = dcompress
        total = unit_total * self._times
        current = unit_total * self._pos[self._len] + accum
        prog = current / total
        return prog, current, total
        # return (1.0 * accum / dcompress + self._pos[self._len]) / self._times

    def next(self):
        if not self.has_next():
            raise IndexError()
        res = list(self._pos[:self._len])
        if self._reverse:
            res = reversed(res)
        self._pos[0] += 1
        for i in range(self._len):
            if self._pos[i] == self._size[i]:
                self._pos[i] = 0
                self._pos[i + 1] += 1
            else:
                break
        return self._type(res)


class NDIndexes:
    def __init__(self, size: int, reverse: bool = True, times: int = 1):
        assert isinstance(size, (list, tuple))
        self._size = size
        self._reverse = reverse
        self._times = times

    def __iter__(self):
        def gen(size, reverse, times):
            _type = type(size)
            _size = size
            _len = len(_size)
            if reverse:
                _size = _type(list(reversed(list(size))))
            pos = [0] * (_len + 1)
            while pos[_len] < times:
                res = list(pos[:_len])
                if reverse:
                    res = reversed(res)
                pos[0] += 1
                for i in range(_len):
                    if pos[i] == _size[i]:
                        pos[i] = 0
                        pos[i + 1] += 1
                    else:
                        break
                yield _type(res)

        return gen(self._size, self._reverse, self._times)
