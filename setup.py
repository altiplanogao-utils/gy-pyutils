#!/usr/bin/env python
from setuptools import find_packages, setup

setup(name='gyutils',
    version='0.1',
    description='A utils',
    author='Gao Yuan',
    packages=find_packages(exclude=("scripts",)),
)