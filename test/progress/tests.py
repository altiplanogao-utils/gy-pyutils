import unittest
from gyutils.progress import *
import sys

class ProgressTestCase(unittest.TestCase):
    def test_iterate_with_progress(self):
        print("start")
        container=[2,4,6,8,10,2,4,6,8,10,2,4,6,8,10,2,4,6,8,10,]
        for i in IterateWithProgress(container, same_line=True, printer=sys.stdout, element_to_str=lambda x:str(x)):
            pass
        print("finish\n")

        print("start")
        for i in IterateWithProgress(container, same_line=False, printer=sys.stdout, element_to_str=lambda x:str(x)):
            pass
        print("finish\n")
