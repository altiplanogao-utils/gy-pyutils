import unittest
from gyutils.choice import Choices

class ChoiceTestCase(unittest.TestCase):

    def test_choice_list(self):
        self.__asserts__(["A","BB","CCC"], ("A","BB","CCC"), (1/3,1/3,1/3))

    def test_choice_list2(self):
        self.__asserts__([("A",1),("BB",2),("CCC",3)], ("A","BB","CCC"), (1/6,1/3,1/2))

    def test_choice_tuple(self):
        self.__asserts__(("A","BB","CCC"), ("A","BB","CCC"), (1/3,1/3,1/3))

    def test_choice_tuple2(self):
        self.__asserts__((("A",1),("BB",2),("CCC",3)), ("A","BB","CCC"), (1/6,1/3,1/2))

    def test_choice_dict(self):
        self.__asserts__({"A" : 1,"BB" : 1,"CCC" : 1}, ("A","BB","CCC"), (1/3,1/3,1/3))

    def test_choice_dict2(self):
        self.__asserts__({"A" : 1,"BB" : 2,"CCC" : 3}, ("A","BB","CCC"), (1/6,1/3,1/2))

    def __asserts__(self, input_options, expect_options, expect_option_weights):
        option_count = len(expect_options)

        choices = Choices(input_options)

        options_no_weight = choices.options(with_weight=False)
        self.assertEqual(option_count, len(options_no_weight))
        self.assertTupleEqual(expect_options, options_no_weight)

        options_weighted = choices.options(with_weight=True)
        self.assertEqual(option_count, len(options_weighted))
        self.assertTupleEqual(tuple(zip(expect_options,expect_option_weights)), options_weighted)

        for i in range(option_count):
            option_a_nw = choices.option(i, with_weight=False)
            self.assertEqual(expect_options[i], option_a_nw)
            option_a_w = choices.option(i, with_weight=True)
            self.assertEqual((expect_options[i], expect_option_weights[i]), option_a_w)
            index_a = choices.option_index(option_a_nw)
            self.assertEqual(i, index_a)

        big_enough = 10000
        votes_dict = {}
        for i in range(big_enough):
            if i % 2 == 0:
                choice_a, a_index = choices.random_choice(with_index=True)
                votes = votes_dict.get(choice_a, 0)
                votes_dict[choice_a] = votes + 1

                index_check = choices.option_index(choice_a)
                self.assertEqual(a_index, index_check)
            else:
                choice_b = choices.random_choice(with_index=False)
                votes = votes_dict.get(choice_b, 0)
                votes_dict[choice_b] = votes + 1

        self.assertTrue(option_count, len(votes_dict))

        min_weight = min(w for o, w in options_weighted)
        tolerance = int(big_enough * min_weight / 10)

        for option, weight in options_weighted:
            expect_votes = int(weight * big_enough)
            diff = abs(votes_dict.get(option) - expect_votes)
            self.assertTrue(diff < tolerance)
